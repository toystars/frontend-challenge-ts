import React, { FC } from 'react';
import { createStyles, makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            width: 'fit-content',
            border: '1px solid #CFCFCF',
            borderRadius: '2px',
            padding: theme.spacing(0.75),
            cursor: 'pointer',
        },
    })
);

interface Props {
    onclick: () => void;
}

export const DeleteIcon: FC<Props> = ({ onclick }) => {
    const classes = useStyles();

    return (
        <Box className={classes.root} onClick={onclick}>
            <DeleteOutlinedIcon color="secondary" />
        </Box>
    );
};
