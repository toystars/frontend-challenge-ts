import { useQuery, useMutation, UseQueryOptions, UseQueryResult, UseMutationOptions, UseMutationResult } from 'react-query';
import axios from 'axios';
import { Ticket } from '../shared/types';
import { queryClient } from '../shared/query-client';

export type TicketsResponse = Ticket[];

const getTicketsApiUrl = (ticketId?: number) => {
    const TICKETS_ENDPOINT = `/api/tickets`;

    if (Boolean(ticketId)) return `${TICKETS_ENDPOINT}/${ticketId}`;
    return TICKETS_ENDPOINT
}

const getTickets = async (): Promise<TicketsResponse> => {
  try {
    const { data } = await axios.get<{ data: Ticket[] }>(getTicketsApiUrl());
    return data.data;
  } catch (e) {
    const errorMessage = e.response?.data?.error?.message || e.message;

    throw new Error(errorMessage);
  }
};

const deleteTicket = async (ticketId: number): Promise<TicketsResponse> => {
    try {
        const { data } = await axios.delete<{ data: Ticket[] }>(getTicketsApiUrl(ticketId));
        return data.data;
    } catch (e) {
        const errorMessage = e.response?.data?.error?.message || e.message;

        throw new Error(errorMessage);
    }
};

export const useGetTickets = (options?: UseQueryOptions<Ticket[]>): UseQueryResult<TicketsResponse> => {
  return useQuery(['GET_TICKETS'], getTickets, options);
};

export const useDeleteTicket = () => {
    return useMutation(
        async ({ ticketId }: { ticketId: number }) => deleteTicket(ticketId),
        {
            onSuccess: () => {
                // invalidate tickets query
                queryClient.invalidateQueries(['GET_TICKETS']);
            }
        }
    );
};
