import React, { FC } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

interface Props {
    isOpen: boolean;
    title: string;
    body: React.ReactNode;
    yesButtonLabel?: string
    noButtonLabel?: string;
    onYesButtonClick?: () => void;
    onNoButtonClick?: () => void;
}

export const ConfirmationDialog: FC<Props> = ({ isOpen, title, body, yesButtonLabel, noButtonLabel, onYesButtonClick, onNoButtonClick }) => {
    return (
        <Dialog
            open={isOpen}
            keepMounted
            aria-labelledby="confirmation-dialog-title"
            aria-describedby="confirmation-dialog-description"
            onClose={onNoButtonClick}
        >
            <DialogTitle id="confirmation-dialog-title">{title}</DialogTitle>
            <DialogContent>
                <DialogContentText id="confirmation-dialog-description">{body}</DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onNoButtonClick} color="secondary">
                    {noButtonLabel ?? 'Cancel'}
                </Button>
                <Button onClick={onYesButtonClick} color="primary">
                    {yesButtonLabel ?? 'Confirm'}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
