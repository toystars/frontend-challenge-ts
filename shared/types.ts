type User = {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
};

export type TicketStatus = 'OPEN' | 'CLOSED';

export type Ticket = {
    id: number;
    createdAt: string;
    updatedAt: string;
    dueDate: string;
    status: TicketStatus;
    userId: string;
    user: User;
};
