import React, { FC, Fragment, useCallback } from 'react';
import Hidden from '@material-ui/core/Hidden';
import { format } from 'date-fns';
import { MobileTicket } from './MobileTicket';
import { DesktopTicket } from './DesktopTicket';
import { Ticket } from '../../shared/types';

const formatToDate = (date: string) => {
    return format(new Date(date), 'dd/MM/yyyy');
};

interface Props {
    ticket: Ticket;
    deleteTicket: (ticket: Ticket) => void;
}

export const ListItem: FC<Props> = ({ ticket, deleteTicket }) => {
    const { id, user, status, createdAt, dueDate } = ticket;

    const createdAtFormatted = formatToDate(createdAt);
    const dueDateFormatted = formatToDate(dueDate);

    const _deleteTicket = useCallback(() => {
        deleteTicket(ticket);
    }, [ticket]);

    return (
        <Fragment>
            {/* Desktop Ticket */}
            <Hidden mdDown>
                <DesktopTicket
                    id={id}
                    user={`${user.firstName} ${user.lastName}`}
                    createdAt={createdAtFormatted}
                    dueAt={dueDateFormatted}
                    status={status}
                    deleteTicket={_deleteTicket}
                />
            </Hidden>
            {/* Mobile Ticket */}
            <Hidden lgUp>
                <MobileTicket
                    id={id}
                    user={`${user.firstName} ${user.lastName}`}
                    createdAt={createdAtFormatted}
                    dueAt={dueDateFormatted}
                    status={status}
                    deleteTicket={_deleteTicket}
                />
            </Hidden>
        </Fragment>
    );
};
