import { QueryClient } from 'react-query';

export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnMount: false,
            refetchOnWindowFocus: false,
            refetchOnReconnect: false,
            retry: false,
            cacheTime: 1000 * 60 * 60,
            staleTime: 1000 * 60 * 5,
        },
    },
});
