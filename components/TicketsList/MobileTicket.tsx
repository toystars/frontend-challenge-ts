import React, { FC } from 'react';
import { createStyles, Grid, makeStyles, Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { Status } from '../Status';
import { DeleteIcon } from '../DeleteIcon';

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            borderBottom: '1px solid #F1F1F1',
            padding: theme.spacing(3, 1.5),
        },
        row: {
            marginBottom: theme.spacing(3.5),
        },
        headerLabel: {
            fontSize: 12,
            lineHeight: '16px',
            fontWeight: theme.typography.fontWeightMedium,
        },
        text: {
            fontWeight: theme.typography.fontWeightLight,
        },
    })
);

interface Props {
    id: number;
    user: string;
    createdAt: string;
    dueAt: string;
    status: string;
    deleteTicket: () => void;
}

export const MobileTicket: FC<Props> = ({ id, user, createdAt, dueAt, status, deleteTicket }) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Grid container className={classes.row}>
                <Grid item xs={6}>
                    <Typography className={classes.headerLabel}>ID</Typography>
                    <Typography variant="body2" className={classes.text}>{id}</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Typography className={classes.headerLabel}>Requested by</Typography>
                    <Typography variant="body2" className={classes.text}>{user}</Typography>
                </Grid>
            </Grid>
            <Grid container className={classes.row}>
                <Grid item xs={6}>
                    <Typography className={classes.headerLabel}>Create date</Typography>
                    <Typography variant="body2" className={classes.text}>{createdAt}</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Typography className={classes.headerLabel}>Due date</Typography>
                    <Typography variant="body2" className={classes.text}>{dueAt}</Typography>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={6}>
                    <Typography className={classes.headerLabel}>Status</Typography>
                    <Status label={status} />
                </Grid>
                <Grid item xs={6}>
                    <DeleteIcon onclick={deleteTicket} />
                </Grid>
            </Grid>
        </Box>
    );
};
