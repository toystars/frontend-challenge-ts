import React, { FC } from 'react';
import { createStyles, Grid, makeStyles, Typography } from '@material-ui/core';
import { Status } from '../Status';
import { DeleteIcon } from '../DeleteIcon';

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            borderBottom: '1px solid #F1F1F1',
            padding: theme.spacing(2, 1.5),
        },
        text: {
            fontWeight: theme.typography.fontWeightLight,
        },
    })
);

interface Props {
    id: number;
    user: string;
    createdAt: string;
    dueAt: string;
    status: string;
    deleteTicket: () => void;
}

export const DesktopTicket: FC<Props> = ({ id, user, createdAt, dueAt, status, deleteTicket }) => {
    const classes = useStyles();

    return (
        <Grid container alignItems="center" className={classes.root}>
            <Grid item xs={1}>
                <Typography className={classes.text}>{id}</Typography>
            </Grid>
            <Grid item xs={3}>
                <Typography className={classes.text}>{user}</Typography>
            </Grid>
            <Grid item xs={2}>
                <Typography className={classes.text}>{createdAt}</Typography>
            </Grid>
            <Grid item xs={2}>
                <Typography className={classes.text}>{dueAt}</Typography>
            </Grid>
            <Grid item xs={2}>
                <Status label={status} />
            </Grid>
            <Grid item xs={2}>
                <DeleteIcon onclick={deleteTicket} />
            </Grid>
        </Grid>
    );
};
