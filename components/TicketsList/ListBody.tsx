import React, { FC, Fragment, useCallback, useState } from 'react';
import { Box, createStyles, makeStyles } from '@material-ui/core';
import { useGetTickets, useDeleteTicket } from '../../hooks/useTickets';
import { CenteredCircularProgress } from '../CenteredCircularProgress';
import { ConfirmationDialog } from '../ConfirmationDialog';
import { NothingFound } from '../NothingFound';
import { ListItem } from './ListItem';
import { Ticket } from '../../shared/types';

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            height: 'calc(100% - 49px)',
            overflowY: 'scroll',
        },
        listItem: {
            borderBottom: '1px solid #F1F1F1',
            padding: theme.spacing(2, 1.5),
        },
    })
);

type ListBodyProps = {};

const ListBody: FC<ListBodyProps> = () => {
    const classes = useStyles();

    const [ticketToDelete, setTicketToDelete] = useState<Ticket | null>(null);
    const { isLoading, data: tickets } = useGetTickets();
    const { mutate: _deleteTicket } = useDeleteTicket();

    const isEmptyContent = !isLoading && !tickets?.length;

    const handleDeleteTicket = useCallback(() => {
        if (!!ticketToDelete) {
            // delete ticket
            _deleteTicket({ ticketId: ticketToDelete.id })
        }
        // close confirmation dialog
        setTicketToDelete(null);
    }, [ticketToDelete]);

    return (
        <Fragment>
            <Box className={classes.root}>
                {isLoading && <CenteredCircularProgress />}
                {isEmptyContent ? <NothingFound /> : tickets?.map((ticket) => (
                    <ListItem
                        key={ticket.id}
                        ticket={ticket}
                        deleteTicket={(ticket) => setTicketToDelete(ticket)}
                    />
                ))}
            </Box>
            <ConfirmationDialog
                isOpen={Boolean(ticketToDelete)}
                title={`Delete Ticket - #${ticketToDelete?.id}`}
                body="Are you sure you want to delete ticket? Action is irreversible."
                yesButtonLabel="Delete"
                onNoButtonClick={() => setTicketToDelete(null)}
                onYesButtonClick={handleDeleteTicket}
            />
        </Fragment>
    );
};

export { ListBody };
