import React, { FC } from 'react';
import classnames from 'classnames';
import { Chip, createStyles, makeStyles } from '@material-ui/core';
import { TicketStatus } from '../shared/types'

const useStyles = makeStyles((theme) =>
    createStyles({
        status: {
            width: '101px',
            height: '19px',
            borderRadius: 4,
            fontSize: 11,
            lineHeight: '15px',
            fontWeight: theme.typography.fontWeightBold,
            color: theme.palette.common.white,
        },
        open: {
            backgroundColor: theme.palette.success.main,
        },
        closed: {
            backgroundColor: theme.palette.error.main,
        }
    })
);

interface Props {
    label: TicketStatus
}

export const Status: FC<Props> = ({ label }) => {
    const classes = useStyles();

    const statusClasses = classnames(classes.status, {
        [`${classes.open}`]: label === 'OPEN',
        [`${classes.closed}`]: label === 'CLOSED',
    })

    return (
        <Chip
            label={label}
            className={statusClasses}
        />
    );
};
